===========================================================================================
/hsg/ - Home Server General FAQ & Guide
v1.00 - Last updated: 2017-10-25

NOTE: Do not take this guide as a rulebook. Feel free to use any pre-existing hardware. This guide is written with beginner and intermediate sysasmins in mind.

Also, CTRL+F and the Table of Contents are your friends.

Welcome to home servers, gentooman.
 
Read this document thoroughly before asking a question in the thread. It should give you a primer on how to select the hardware you need and configuring the OS for ideal operation.
 
If you came here for the miscellaneous links, go to the bottom sections.
 
Check the OP for Discord and previous thread:
Permanent link: https://boards.4chan.org/g/hsg
Discord: https://discord.gg/9vZzCYz
 
If you have any feedback, suggestions on things to add, edit, or remove, please reply to the OP with it. We'll try to check it frequently to keep this updated. It's easier than browsing the thread looking for suggestions. In addition, any contributions/submissions are highly appreciated.
 
===========================================================================================
Table of Contents
[FAQ]
-------- What is a home server?
-------- Is it worth to own one?
[BUILD SUGGESTIONS]
-------- Consumer level (Synology/QNAP/other NAS)
-------- Entry level (Single-board computers)
-------- Intermediate Level (repurposed desktops)
-------- (Semi-)professional Level (Xeon, enterprise hardware)
[GENERAL TIPS]
-------- "I just bought a used server."
-------- UPS (uninterruptible power supplies)
-------- Setting up HTTPS / Issuing Certificates
-------- 
[CONFIGURATION]
-------- Server Settings
-------- BIOS Settings
[VIDEOS]
-------- Server Tips
-------- Miscellaneous
[LINKS]
-------- Useful links
-------- Miscellaneous
[CHANGELOG]
 
===========================================================================================
[FAQ]
 
-------- What is a home server?

A home server is a computing server located in a private residence providing services to other devices inside or outside the household through a home network or the Internet. Such services may include file and printer serving, media center serving, web serving (on the network or Internet), web caching, account authentication and backup services. Because of the relatively low number of computers on a typical home network, a home server commonly does not require significant computing power and can be implemented with a re-purposed, older computer, or a plug computer. An uninterruptible power supply is sometimes used in case of power outages that can possibly corrupt data.
 
-------- Is it worth to own one?
 
Hosting your website on a home server can be done if you know what you're actually doing. But if you have to spend so much time learning and buying new equipment from scratch, perhaps you need to assess if all this is worth the trouble. Most growing businesses would rather rent web servers from professional web hosts on a monthly or yearly basis because they find that it much cheaper to do so. However if you prefer to learn how to do all this yourself whilst remaining in control over the hardware and the content you serve in the way you please, a home server is the better option.


===========================================================================================
[BUILD SUGGESTIONS]

What follows are descriptions on the basic build options you might have when getting into owning your own home server. Discussion about their hardware mostly falls under the questions of what you want to achieve with it and consider more important. For info on this, check the [FAQ] section above for a general answer.
 
-------- Consumer level (Synology/QNAP/other NAS)

Good for basic protocol support. Limited in what you might be able to run. If you just want to own a file server you'll find plenty of in-built options:
HTTP/HTTPS/SMB/FTP

For around 150-250 bucks you can saturate a 1 Gbit/s ethernet and get a good dual bay device without drives.

-------- Entry level (Single-board computers)

For the poor of us. For just $40 you can have a Raspberry Pi 3 with USB-PSU, case and cooling pad.
Additional cost may be:
SD card
USB to SATA adapter
External 2.5" drive, SSD or HDD

You might need a stronger power supply or a powered USB hub depending on what kind of drive and how many you want to hook up.

This is the weakest of all servers, but it suffices for streaming properly encoded full-HD content.

Install Debian, Ubuntu server or even RasPlex.

-------- Intermediate Level (repurposed desktops)


-------- (Semi-)professional Level (dual Xeon, enterprise hardware, etc)




===========================================================================================
[GENERAL TIPS]
 
-------- "I just bought a used server."

-------- UPS (uninterruptible power supplies)


-------- Setting up HTTPS / Issuing Certificates

https://github.com/hlandau/acme/blob/master/README.md

 
===========================================================================================
[CONFIGURATION]

-------- Server Settings

1) You should create a private RSA-4096 key for remote access.
2) Export your rsa-pubkey
3) Use vi/vim/nano to edit it into .ssh/authorized_keys
4) Configure sshd_config accordingly to only allow pubkey authentication.

-------- BIOS Settings
 
===========================================================================================
[VIDEOS]

-------- Server Tips

-------- Miscellaneous

===========================================================================================
[LINKS]

-------- Useful links

-------- Miscellaneous
 
===========================================================================================
[CHANGELOG]

v1.00 - initial draft released
 
===========================================================================================