#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------Author : <r@$h0v3rr!d3 -----------"
echo "-----------Usage : Run this command where the file is located without quotes 'bash ml_big_data_softwares_installation.sh-----------"
echo "-----------IMPORTANT NOTE - Please read the comments before the comments to know what you're installing.-----------"

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------This script will install all the updated required libraries listed below to start up on machine learning and big data for distributed computing 
#1. opencv
#2. opengl
#3. scikit-learn
#4. keras
#5. tensorflow
#6. openmp
#7. openmpi
#8. atlas and lapack
#9. armadillo
#10. mlpack
#11. anaconda
#12. caffe2
#13. zip file of caffe because caffe needs some editing manually inside it's Makefile.conf file-----------"

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------To update your system before installing these softwares, uncomment below line and run this-----------"
#echo password | sudo -S apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y && sudo apt autoremove -y && sudo apt autoclean -y

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------Installing pre-requisite softwares to install the above libraries (some commands may be repeated don't worry about it, it's safe)-----------"
sudo apt-get install -y python-devel numpy gtk2-devel libv4l-devel ffmpeg-devel gstreamer-plugins-base-devel
sudo apt-get install -y libpng-devel libwebp-devel libjpeg-turbo-devel jasper-devel openexr-devel libtiff-devel
sudo apt-get install -y p7zip p7zip-full unrar-free unzip tar zip htop
sudo apt-get install -y faketime htop lshw wget
sudo apt-get install -y screen nano vim geany terminator
sudo apt-get install -y build-essential checkinstall cmake git libopencv-dev libgtk2.0-dev pkg-config libavcodec-dev libpng12-dev libavformat-dev libswscale-dev yasm libxine2 libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libqt4-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils
sudo apt-get install -y gcc gcc-c++ g++
sudo apt-get install -y gdebi
sudo apt-get install -y qt5-default qtcreator qml-module-qtquick2 libqt5webkit5-dev libqt5svg5-dev
sudo apt-get install -y gparted pitivi
sudo apt-get install -y network-manager-openvpn network-manager-openvpn-gnome transmission-gtk
sudo apt-get install -y blender gimp imagemagick inkscape pavucontrol audacity libreoffice default-jdk ant
sudo apt-get install -y libav-tools vlc build-essential cmake
sudo apt-get install -y qt5-default libvtk6-dev
sudo apt-get install -y zlib1g-dev libjpeg-dev libwebp-dev libpng-dev libtiff5-dev libjasper-dev libopenexr-dev libgdal-dev
sudo apt-get install -y libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev yasm libopencore-amrnb-dev libopencore-amrwb-dev libv4l-dev libxine2-dev
sudo apt-get install -y libtbb-dev libeigen3-dev
sudo apt-get install -y python-dev python-tk python-numpy python3-dev python3-tk python3-numpy
sudo apt-get install -y gnuplot gnuplot-doc gnuplot-qt python-gnuplot liblualib50-dev xfig xfig-doc transfig fig2ps 
sudo apt-get install -y imagemagick netpbm mjpegtools pdftk giftrans gv evince smpeg-plaympeg mplayer totem ffmpeg libav-tools eog
sudo apt-get install -y unzip wget doxygen
sudo apt-get install -y texinfo texlive gnome-terminal libreoffice unoconv libreoffice-dmaths libbz2-dev libncurses5-dev curl a2ps wdiff
sudo apt-get install -y python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
sudo apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install --assume-yes libopencv-dev build-essential cmake git libgtk2.0-dev pkg-config python-dev python-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libtbb-dev libqt4-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip
sudo apt-get install -y ffmpeg libopencv-dev libgtk-3-dev python-numpy python3-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libv4l-dev libtbb-dev qtbase5-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip
sudo apt-get install -y libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
sudo apt-get install -y libboost-all-dev
sudo apt-get install -y libopenblas-dev libatlas-base-dev
sudo apt-get install -y libgflags-dev libgoogle-glog-dev liblmdb-dev
sudo apt install -y tesseract-ocr libopenblas-* ceres-solver-doc
sudo apt-get install -y emacs python-mode gedit vim ispell gawk f2c gcc g++ fortran autoconf automake autotools-dev libatlas-base-dev libsuitesparse-dev subversion mercurial cvs ranger mpv idle python-pip python-setuptools python-dev libfreetype6-dev libpng-dev numpy sympy cython swig python-matplotlib
source ~/.bashrc

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------Installing latest opencv-----------"
echo "-----------if you want to install older opencv follow these below links-----------"
echo "-----------http://leoybkim.com/wiki/installing-opencv-2.4.13-on-ubunto-16.04/-----------"
echo "-----------https://www.learnopencv.com/install-opencv3-on-ubuntu/-----------"

git clone https://github.com/Itseez/opencv.git
git clone https://github.com/Itseez/opencv_contrib.git
cd opencv
mkdir libs
mkdir build
cd build

echo "-----------If you dont have a graphic card or cuda or nvidia softwares installed, run the below command else run the other cmake mentioned-----------"
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=ON -D ENABLE_PRECOMPILED_HEADERS=OFF -D WITH_OPENGL=ON -D FORCE_VTK=ON -D INSTALL_PYTHON_EXAMPLES=ON -D WITH_TBB=ON -D WITH_V4L=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules -D BUILD_EXAMPLES=ON ..

echo "-----------If you have both cuda and nvidia softwares installed, uncomment below software and comment the above one-----------"
#cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_CUDA=ON -D WITH_IPP=ON -D WITH_CUBLAS=ON -D ENABLE_PRECOMPILED_HEADERS=OFF -D BUILD_TIFF=ON -D WITH_TBB=ON -D FORCE_VTK=ON -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF -D OPENCV_EXTRA_MODULES_PATH= ../../opencv_contrib/modules -D BUILD_EXAMPLES=OFF -D WITH_GDAL=ON -D WITH_XINE=ON -D WITH_QT=ON -D WITH_V4L=OFF -D WITH_OPENGL=ON .. -D CUDA_NVCC_FLAGS="-D_FORCE_INLINES" ..


make -j4 #4 means number of processors, which can be changed by running nproc in your terminal and changing it to "make -j32" or "make -j16" etc
make install
sudo make install
sudo sh -c 'echo "/usr/local/lib" >> /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

echo "-----------Installing opencv_extra which has additional opencv programs for multiple languages to try out-----------"
cd ../../
git clone https://github.com/Itseez/opencv_extra.git

echo "-----------Sanity checks to see if opencv is installed which just shows the path where it's installed-----------"
pkg-config --cflags opencv
pkg-config --libs opencv
find /usr/local/lib/ -type f -name "cv2*.so"
whereis opencv
whereis opencv2
whereis opencv3
whereis opencv4

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------Installing pre-requisite pip scientific software support-----------"

pip install scipy ipython pyzmq nose pytest sphinx pydb  python-profiler python-dev spyder 

echo "-----------Installing scikit-learn-----------"
pip install scikit-learn

echo "-----------Installing keras-----------"
pip install keras

echo "-----------Installing tensorflow-----------"
pip install tensorflow

echo "-----------Installing numpy-----------"
pip install numpy

echo "-----------Installing matplotlib-----------"
pip install matplotlib

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------Installing openmpi and openmp-----------"
sudo apt-get install -y libatlas* liblapack* python-igraph python-opencv python-opengl
sudo apt-get install -y libboost-math-dev libboost-program-options-dev libboost-random-dev
sudo apt-get install -y openmpi-* mpi*

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "Installing armadillo"
sudo apt-get install -y liblapack-dev libblas-dev libboost-dev libarmadillo-dev
git clone git clone https://gitlab.com/conradsnicta/armadillo-code.git
wget http://sourceforge.net/projects/arma/files/armadillo-9.100.5.tar.xz
tar -xvf armadillo-9.100.5.tar.xz
cd armadillo-9.100.5
cmake .
make -j4
sudo make install
cd ..


#---------------------------------------------------------------------------------------------------------------------------------------------

echo "Installing mlpack"
sudo apt-get install -y libmlpack-dev libboost-all-dev
sudo apt-get install -y  libboost-test-dev libboost-serialization-dev libarmadillo-dev binutils-dev python-pandas python-numpy cython python-setuptools
wget http://www.mlpack.org/files/mlpack-3.0.3.tar.gz
tar -xvzpf mlpack-3.0.3.tar.gz
mkdir mlpack-3.0.3/build && cd mlpack-3.0.3/build
cmake ../
make -j4
sudo make install
cd ..




#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------Installing anaconda and miniconda-----------"
wget https://repo.anaconda.com/archive/Anaconda2-5.2.0-Linux-x86_64.sh
bash Anaconda2-5.2.0-Linux-x86_64.sh
wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh
source ~/.bashrc
conda update --all
conda update conda
conda update anaconda


#---------------------------------------------------------------------------------------------------------------------------------------------

echo "Installing caffe2"
conda install -c conda-forge opencv
conda install -c conda-forge/label/broken opencv 
conda install theano
conda install -c conda-forge tensorflow

echo "if you dont have gpu or nvidia cudnn softwares run the below command else other ones listed after it"
conda install -c caffe2 caffe2
echo "For Caffe2 with CUDA 9 and CuDNN 7 support"
#conda install -c caffe2 caffe2-cuda9.0-cudnn7
echo "For Caffe2 with CUDA 8 and CuDNN 7 support"
#conda install -c caffe2 caffe2-cuda8.0-cudnn7

#---------------------------------------------------------------------------------------------------------------------------------------------

echo "-----------Installing caffe-----------"
git clone http://github.com/BVLC/caffe.git
cd caffe
cp Makefile.config.example Makefile.config

echo "-----------Open the Makefile.config file using any text editor and only uncomment these lines.-----------"

#cuDNN acceleration switch (uncomment to build with cuDNN).
#USE_CUDNN := 1

# Uncomment if youre using OpenCV 3
#OPENCV_VERSION := 3

# We need to be able to find Python.h and numpy/arrayobject.h.
#PYTHON_INCLUDE := /usr/include/python2.7 \
#        /usr/local/lib/python2.7/dist-packages/numpy/core/include

# Uncomment to support layers written in Python (will link against    Python libs)
#WITH_PYTHON_LAYER := 1

# Whatever else you find you need goes here.
#INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include /usr/include/hdf5/serial
#LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib /usr/lib/x86_64-linux-gnu/hdf5/serial/

echo "-----------Now run this command to build caffe with OpenCV-----------"

echo "-----------make clean && make all -j10 & make test -j10 && make runtest -j10 && make pycaffe -j10-----------"


echo "-----------Now add these libraries path to your ~/.bashrc where it looks like this-----------"

# Opencv
#export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
#export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/:$PKG_CONFIG_PATH

# Caffe path where you installed it add it below at /home...
#export PYTHONPATH="/home/username/Downloads/caffe-master/python/":$PYTHONPATH

#---------------------------------------------------------------------------------------------------------------------------------------------

#Now source ~/.bashrc to include all the paths
source ~/.bashrc
