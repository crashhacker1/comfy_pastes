alias cd=git_cd
git_cd() {
    builtin cd "$@" && { [ ! -d .git ] || { git fetch origin; } }
}

alias ls='ls --color=auto'
alias weather='curl wttr.in/~muh_sweet_home'
alias reboot='sudo reboot'
alias shutdown='sudo shutdown -h now'
alias sf='screenfetch'
alias l.='ls -d .*'
alias lsq='ls -a | grep'
alias t='tmux'
alias ta='tmux attach'
alias td='tmux detach'
alias folders='du -h --max-depth=1 | sort -hr'
alias youtube-download="youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4'"
alias get_space="sudo du -sh ./*"
alias update="echo password | sudo -S apt-get update && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt-get autoclean -y && sudo apt-get autoremove -y"
alias tstart="sudo tlp start"
alias ytv="youtube-dl --add-metadeta -ic"
alias yta="youtube-dl --add-metadata -xic"
alias es="speedometer -r enp11s0"
alias wf="speedometer -r wlp2s0"
alias 4get="wget -nd -r -l 1 -H -D i.4cdn.org -A png,gif,jpg,jpeg,webm,pdf"

export TERM=xterm-256color

PS1="\n\[\e[0;37m\]┌─[\[\e[1;32m\u\e[0;37m\]]──[\e[1;33m\w\e[0;34m]\[\e[0;34m\]\[\e[1;36m\] $_BRANCH_STR: \$ \[\e[0;37m\]\n|\n\[\e[0;37m\]└────╼ \[\e[1;36m\]>>> \[\e[00;00m\]"

source ~/.bash_profile;