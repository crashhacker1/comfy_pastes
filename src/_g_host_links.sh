#+TITLE: Interestin/g/ Links
#+STARTUP: showall

Interesting links
* Blogs
  [[http://studiopixel.sakura.ne.jp/][Cavestory devblog]]
  [[http://toastytech.com/index.html][Nathan Lineback and his 20+ year crusade against Internet Explorer]]
  [[http://www.fromthemachine.org/MALOVIOUS.html][Ramblings about current events]]
  [[https://omaera.org/][Anons from Chile personal site, very minimalist]]
  [[http://9ch.in/][Oldfag personal site]]
  [[https://melonking.net/melon.html][Irish compsci bizarre blog]]
  [[http://www.t-s-k-b.com/][Japanese surreal artist blog]]
  [[https://neko.space/][Cyberpunkish blog]]
  [[https://fuwafuwa.moe/][Sysadmin from Belgium, got charged with CP after someone abused his free file hosting service]]

* Services
  [[https://wiby.me][Simple website search engine]]
  [[http://gen.lib.rus.ec/][Large e-book database, textbooks and literature]]
  [[https://sci-hub.tw/][Scientific paper paywall unlocker]]
  [[https://lysergi.com/][Trusted research chemical dealer]]
  [[https://lizardlabs.eu/][Sketchy designer drugs]]
  [[http://tilde.town/][Direct connection (ssh) social media]]

* Messageboards 
  [[https://systemspace.link/][Lain / cyberpunk messageboard]]
  [[http://4-ch.net/4ch][Pretty dead ancient tier textboard]]
  [[https://dangeru.us/][Relatively active textboard originally based on a VN called VA-11 Hall-A]]
  [[https://arisuchan.jp/][Some kind of lainchan spinoff that I don't remember the story for]]
  [[https://sushigirl.us/][Imageboard with only a few posts per day but not entirely dead]]
  [[https://doushio.com/moe/][Run of the mill imageboard but you can see anons type out posts in real time. Decently active]]
  [[https://meguca.org/][Another imageboard using the same script as doushio. Also active]]
  [[https://chakai.org/tea/][Yet another imageboard using the real time script, but this one is only available at certain times during the day. It's active but seems to be populated by only a few users that all know each other.]]
  [[http://afternoon.dynu.com/letterbox.html][Simple text based board, very chill]]
  [[http://www.pown.it/][Text board based on random threads]]
  [[http://4x13.net/list/][List of sorted active boards about programming and usual internet biz]]
  [[http://www.txtchan.org/][Catalog of somewhat active "2ch style" boards]]
  [[http://4taba.net/][Relatively active board about bizarre subjects]]
  [[https://chan.org.il][Kosher friendly board]]

* Random
  [[https://kongoucheats.com/][Background video]]
  [[http://bird.lolis.download/][Avant-garde hippy site]]
  [[https://motherfuckingwebsite.com/][Simple design philosophy for motherfucking websites]]
  [[http://bettermotherfuckingwebsite.com/][A simple design philosophy for even better motherfucking websites]]
  [[http://salmonofcapistrano.com/][Parallax craziness]]
  [[http://www.darkroastedblend.com/][Random interesting articles]]
  [[http://wwwwwwwww.jodi.org][Retro looking none sense]]
  [[http://fusionanomaly.net/nodebase.html][Psychedelic personal wiki?]]
  [[https://www.cameronsworld.net/][Messy web 1.0 styled blog]]
  [[https://www.bibliotecapleyades.net/][Spanish collections of random articles]]
  [[http://vampirewebsite.net/][Edgy websites about real vampires]]                             
  [[http://cachemonet.com/][Dumb animations]]
  [[http://nobodytm.com/][Album promo site?]]
  [[https://yyyyyyy.info/][HTML nonesense]]
  [[https://www.lingscars.com/][Very colorful car rental site]]
  [[http://dankmaymays.com/bean/][Wonky animation with sound]]
  [[http://project.cyberpunk.ru/][Cyberpunk files and info]]
  [[http://jollo.org/LNT/map/][Some IRC user site]]
  [[http://harmful.cat-v.org/][Parody articles and journals]]
  [[https://forgottenlanguages-full.forgottenlanguages.org/][Info about semantics and esoterism]]
  [[http://currentcondition.org/][Stylish weather broadcaster]]
  [[http://daggermag.com/][Internet magazine]]                                   
  [[http://www.oldradioworld.com/media/][Long list of media]]
  [[http://www.actsofgord.com/Wrath/chapter01.php][Parody cult]]
  [[http://timecube.2enp.com/][Crazy writings about conspiracies]]
  [[http://www.cavernsofblood.com][Oldschool point and click web adventure]]
  [[http://www.heavensgate.com/][Infamous suicide cult frontsite]]
  [[https://maps.vlasenko.net/soviet-military-topographic-map/map200k.html][Oldcomputers soviet maps]]
  [[http://bluemaxima.org/flashpoint/][34.4GB archive of old Flash games]]
  [[http://www.smbmovie.com/][Fan site for the live action Super Mario Bros. movie]]
  [[http://www.deceptionisland.aq/][Official website for Deception Island, an Antarctic island]]
  [[http://ilovejarjarbinks.tripod.com/][Jar Jar Binks fan page, seriously]]
  [[https://templeos.holyc.xyz/][TempleOS backup site]]
  [[https://www.yyyyyyy.info/][HTML insanity and randomness]]
  [[https://liooil.space/][Psychedelic inspired collage]]
  [[http://zoomquilt.org/][Fractal zoom in]]
  [[http://turnyournameintoaface.com][Turns your name into an ASCII art face]]
  [[https://mebious.neocities.org/][Matrix / Lain inspired designs]]
  [[https://www.e-corp-online.com/][Window 9X styled cyberpunk site]]
  [[www.cyberpunk.ru][Moscos cyberpunk scene from the early 2000s]]

* Information
  [[https://tcrf.net/The_Cutting_Room_Floor][Unused and cut video game content]]
  [[https://proofwiki.org/wiki/Main_Page][Mathematical proofs]]
  [[https://erowid.org/][Psychedelics  and their effects]]
  [[https://jeffpar.github.io/kbarchive/][Early Microsoft articles]]
  [[https://n-o-d-e.net/index.html][Tons of cybersecurity articles]]
  [[https://www.malware-traffic-analysis.net][Network packet analysis and malware recon]]
  [[http://ultra64.ca/][Nintendo64 information, dev tools etc.]]

* FTP and Filetrees
  [[http://dreamatico.com/data_images/][Sorted weird images]]
  [[ftp://50.31.112.231/][Fonts and oldschool cyberpunk literature, bunch of cool dungeons and dragons stuff hidden in there]]
  [[ftp://ftp.sai.msu.su/][Old LINUX scripts and text files]]
  [[ftp://cyberia.dnsalias.com/pub/filebase/][Old software like games data collections]]
  [[ftp://ftp.dinoex.net/pub/][ROMS and software]]                             
  [[http://thewatsonbrothers.com/files/][Random files from ~2005]]
  [[http://ftpmirror.your.org/pub/misc/ftp.microsoft.com/][Windows files and logs]]


* Collections
  [[http://keygenmusic.net/][Keygen beats and oldschool mixes]]
  [[http://oldcomputers.net/][Old hardware listing]]
  [[http://www.doshaven.eu/][DOS games and applications]]
  [[http://textfiles.com/][Massive collection of very old text files, from the BBS and early email days]]
  [[http://The-eye.eu][Old roms, books, murdercube stuff etc]]
  [[https://node.neko.space/s/library][Engineering and compsci books]]
  [[https://browsers.evolt.org/][Evolt Browser Archive – lots of obscure and forgotten browsers here]]
  [[http://hhug.me/][ROM dumps of bootleg Game Boy/Game Boy Advance games]]
  [[http://cah4e3.shedevr.org.ru/][ROM dumps of bootleg Famicom games]]
  [[http://www.ifindit.com/][Forgotton web directory, last updated 1998]]
  

* Pastebins
  [[https://pastebin.com/EUrfHx7n][List of chans]]

* SSH
  ssh memes@whisper.onthewifi.com ## Animated smug pepe, password=memes